package omgcode.swagger.service;

import omgcode.swagger.dto.EmployeeDTO;
import omgcode.swagger.entity.Employee;
import omgcode.swagger.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;


    public void persistEmployee(EmployeeDTO employeeDTO) {

        this.employeeRepository.save(Employee.builder()
                .name(employeeDTO.getName())
                .salary(employeeDTO.getSalary())
                .description(employeeDTO.getDescription())
                .build());
    }

    public EmployeeDTO getEmployeeInfo(String id) {
        Employee employee = this.employeeRepository.getReferenceById(Integer.valueOf(id));
        return EmployeeDTO.builder()
                .name(employee.getName())
                .salary(employee.getSalary())
                .description(employee.getDescription())
                .build();
    }
}
