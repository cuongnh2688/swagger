package omgcode.swagger.controller;

import io.swagger.annotations.Api;
import omgcode.swagger.dto.EmployeeDTO;
import omgcode.swagger.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")
@Api(value = "User APIs")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/save")
    public ResponseEntity<String> saveEmployee(@RequestBody EmployeeDTO employeeDTO) {

        this.employeeService.persistEmployee(employeeDTO);

        return ResponseEntity.ok("create an employee successfully!");
    }

    @RequestMapping("/info/{id}")
    public ResponseEntity<EmployeeDTO> getEmployeeInfo(@PathVariable("id") String id){
        return ResponseEntity.ok( this.employeeService.getEmployeeInfo(id));
    }
}
